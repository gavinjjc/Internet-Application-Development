<?php
  // PHP file which calls queries to use in product_table.html
  // Author: Gavin Connorton

  // Server login information
  $servername = "student.computing.dcu.ie";
  $username   = "user689";
  $password   = "pw1243";
  $DBName     = "project3";

  // Establish Connection
  $connect = new mysqli ($servername, $username, $password, $DBName);

  // Failed Connection Message
  if(! $connect )
  {
    die('Could not connect: ' . mysql_error());
  }
  echo "Connected successfully";
  echo "<br>";

  // Form Values
  $product_num  = $_POST['Product_Num'];
  $product_name = $_POST['Product_Name'];
  $amt_in_stock = $_POST['Amt_In_Stock'];
  $item_num     = $_POST['Item_Num'];

  if(isset($_POST['update'])) // Values from form
  {
    // SQL Query for 60%, not that creative
    $query       = "INSERT INTO Product_Table (Product_Num, Product_Name, Amt_In_Stock, Item_Num) VALUES ('$product_num','$product_name','$amt_in_stock','$item_num') ";
    $returnvalue = mysqli_query($connect , $query);

    // Return table
    $query2    = "SELECT * FROM Product_Table";
    $resultset = mysqli_query($connect, $query2);

    // Return Message
    if ($returnvalue)
    {
      echo "New record created successfully";
      echo "<table>
              <caption>Product Table</caption>
              <tr><th scope='col'>Product ID</th>
                  <th scope='col'>Product Name</th>
                  <th scope='col'>Amount In Stock</th>
                  <th scope='col'>Item Number</th>
              </tr>";

      while($row = mysqli_fetch_assoc($resultset))
      {
        echo "<tr scope='row'><td>" . $row["Product_Num"]   . "</td><td>" .
                                      $row["Product_Name"]  . "</td><td>" .
                                      $row["Amt_In_Stock"]  . "</td><td>" .
                                      $row["Item_Num"]      . "</td></tr>" ;
      }
      echo "</table>";
    }
    // Error message if data can't be updated
    elseif (! $returnvalue)
    {
      echo "ERROR: Could not execute $query. " . mysqli_error($connect);
    }

    echo mysqli_error($resultset);
    mysqli_close($connect);
  }
  elseif(isset($_POST['delete'])) // Values from form
  {
    // SQL Query for extra marks, slightly ceative
    $query       = "DELETE FROM `$DBName`.`Product_Table` WHERE `Product_Table`.`Product_Num` = $product_num";
    $returnvalue = mysqli_query($connect , $query);

    // Return table
    $query2    = "SELECT * FROM Product_Table";
    $resultset = mysqli_query($connect, $query2);

    // Return Message
    if ($returnvalue)
    {
      echo "Record deleted successfully";
      echo "<table>
              <caption>Product Table</caption>
              <tr><th scope='col'>Product ID</th>
                  <th scope='col'>Product Name</th>
                  <th scope='col'>Amount In Stock</th>
                  <th scope='col'>Item Number</th>
              </tr>";

      while($row = mysqli_fetch_assoc($resultset))
      {
        echo "<tr scope='row'><td>" . $row["Product_Num"]   . "</td><td>" .
                                      $row["Product_Name"]  . "</td><td>" .
                                      $row["Amt_In_Stock"]  . "</td><td>" .
                                      $row["Item_Num"]      . "</td></tr>" ;
      }
      echo "</table>";
    }
    // Error message if data can't be deleted
    elseif (! $returnvalue)
    {
      echo "ERROR: Could not execute $query. " . mysqli_error($connect);
    }

    echo mysqli_error($resultset);
    mysqli_close($connect);
  }
?>
